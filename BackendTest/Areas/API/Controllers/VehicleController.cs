﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BackendTest.Areas.API.Models;
using BackendTest.Domain.Services;

namespace BackendTest.Areas.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IVehicleService _vehicleService;

        public VehicleController(IVehicleService vehicleService)
        {
            _vehicleService = vehicleService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleDto>>> GetVehicleDto()
        {
            var listVehicles = await _vehicleService.GetVehiclesAsync();

            return Ok(listVehicles);
        }

        [HttpPost]
        public async Task<ActionResult> PostVehicleDto(VehicleDto vehicleDto)
        {
            var newVehicle = await _vehicleService.CreateVehicleAsync(vehicleDto.VIN, vehicleDto.LicensePlateNumber, vehicleDto.Model, 
                vehicleDto.BrandId, vehicleDto.FuelId, vehicleDto.ColorId, vehicleDto.VehicleEquipmentId, vehicleDto.Year);

            return CreatedAtAction("GetVehicleDto", new { id = newVehicle.Id }, newVehicle);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> PutVehicleDto(int id, VehicleDto vehicleDto)
        {
            if (id != vehicleDto.Id)
            {
                return BadRequest();
            }

            await _vehicleService.UpdateVehicleAsync(id, vehicleDto.VIN, vehicleDto.LicensePlateNumber, vehicleDto.Model, 
                vehicleDto.BrandId, vehicleDto.FuelId, vehicleDto.ColorId, vehicleDto.VehicleEquipmentId, vehicleDto.Year);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteVehicleDto(int id)
        {
            var deletedVehicle = await _vehicleService.DeleteVehicleAsync(id);

            if (deletedVehicle)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}
