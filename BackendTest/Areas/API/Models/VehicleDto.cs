﻿namespace BackendTest.Areas.API.Models
{
    public class VehicleDto
    {
        public int Id { get; set; }
        public string VIN { get; set; }
        public string LicensePlateNumber { get; set; }
        public string Model { get; set; }
        public int BrandId { get; set; }
        public int FuelId { get; set; }
        public int ColorId { get; set; }

        //public IEnumerable<VehicleEquipment> VehicleEquipments { get; set; }
        public int VehicleEquipmentId { get; set; }
        public int Year { get; set; }
    }
}
