﻿namespace BackendTest.Domain.Entities
{
    public class VehicleEquipment
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
