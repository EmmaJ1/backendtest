﻿namespace BackendTest.Domain.Entities
{
    public class Fuel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
