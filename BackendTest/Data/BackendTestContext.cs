﻿using BackendTest.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BackendTest.Data
{
    public class BackendTestContext : DbContext
    {
        public BackendTestContext(DbContextOptions<BackendTestContext> options) : base(options)
        {
        }

        public DbSet<Brand> Brands { get; }
        public DbSet<Color> Colors { get; }
        public DbSet<Fuel> Fuels { get; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<VehicleEquipment> VehicleEquipments { get; }
    }
}
