﻿using System.ComponentModel.DataAnnotations;

namespace BackendTest.Domain.Entities
{
    public class Vehicle
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "VIN is required")]
        [StringLength(17)]
        public string VIN { get; set; }

        [Required(ErrorMessage = "LicensePlateNumber is required")]
        [StringLength(6)]
        public string LicensePlateNumber { get; set; }
        public string Model { get; set;}
        public int BrandId { get; set; }
        public Brand Brand { get; set; }
        public int FuelId { get; set; }
        public Fuel Fuel { get; set; }
        public int ColorId { get; set; }
        public Color Color { get; set; }
        //public IEnumerable<VehicleEquipment> VehicleEquipments { get; set; }
        public int VehicleEquipmentId { get; set; }
        public VehicleEquipment VehicleEquipment { get; set; }
        public int Year { get; set; }
    }
}
