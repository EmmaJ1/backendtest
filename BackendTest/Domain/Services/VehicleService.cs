﻿using System.Collections.Generic;
using BackendTest.Data;
using BackendTest.Domain.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace BackendTest.Domain.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly BackendTestContext _context;

        public VehicleService(BackendTestContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Vehicle>> GetVehiclesAsync()
        {
            return await _context.Vehicles.ToListAsync();
        }

        public async Task<Vehicle> CreateVehicleAsync(string vIN, string licensePlateNumber, string model, int brandId,
            int fuelId, int colorId, int vehicleEquipmentId, int year)
        {
            var newVehicle = new Vehicle
            {
                VIN = vIN,
                LicensePlateNumber = licensePlateNumber,
                Model = model,
                BrandId = brandId,
                FuelId = fuelId,
                ColorId = colorId,
                VehicleEquipmentId = vehicleEquipmentId,
                //VehicleEquipments = new IEnumerable<VehicleEquipment>(vehicleEquipments),
                Year = year
            };

            //newVehicle.VehicleEquipments = vehicleEquipments;

            //foreach(VehicleEquipment v in vehicleEquipments)
            //{
            //    newVehicle.VehicleEquipments.add(v);
            //}

            _context.Vehicles.Add(newVehicle);
            await _context.SaveChangesAsync();

            return newVehicle;
        }

        public async Task<bool> DeleteVehicleAsync(int id)
        {
            var vehicle = _context.Vehicles.FirstOrDefault(x => x.Id == id);

            if (vehicle == null)
            {
                return await Task.FromResult(false);
            }

            _context.Vehicles.Remove(vehicle);
            var recordsAffected = await _context.SaveChangesAsync();

            return recordsAffected > 0;
        }

        public async Task<Vehicle> UpdateVehicleAsync(int vehicleId, string vIN, string licensePlateNumber, string model, int brandId,
            int fuelId, int colorId, int vehicleEquipmentId, int year)
        {
            var oldVehicle = await _context.Vehicles.FindAsync(vehicleId);

            oldVehicle.VIN = vIN;
            oldVehicle.LicensePlateNumber = licensePlateNumber;
            oldVehicle.Model = model;
            oldVehicle.BrandId = brandId;
            oldVehicle.FuelId = fuelId;
            oldVehicle.ColorId = colorId;
            oldVehicle.VehicleEquipmentId = vehicleEquipmentId;
            oldVehicle.Year = year;

            _context.Vehicles.Update(oldVehicle);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }

            return oldVehicle;
        }
    }
}
