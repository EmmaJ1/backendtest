﻿using System.Collections.Generic;
using BackendTest.Domain.Entities;
using System.Threading.Tasks;

namespace BackendTest.Domain.Services
{
    public interface IVehicleService
    {
        Task<IEnumerable<Vehicle>> GetVehiclesAsync();
        Task<Vehicle> CreateVehicleAsync(string vIN, string licensePlateNumber, string model, int brandId, 
            int fuelId, int colorId, int vehicleEquipmentId, int year);
        Task<bool> DeleteVehicleAsync(int id);
        Task<Vehicle> UpdateVehicleAsync(int vehicleId, string vIN, string licensePlateNumber, string model, int brandId,
            int fuelId, int colorId, int vehicleEquipmentId, int year);
    }
}
